Мы &mdash; IT-изба, ребята которые обеспечивают IT-поддержку проекту &laquo;[Летняя школа](https://letnyayashkola.org)&raquo; последние 10+ лет.
Нас мало, но мы продолжаем это делать.
Скорее всего если вы хотя бы раз что-то делали на ЛШ, так или иначе наши сервисы поучаствовали в этом, будь это почта, боты, сайт или анкета участника.

Почему мы это делаем?
Потому что на самом деле это весело.
Нам очень не хватает рук и мы верим что среди вас есть те, кто хочет разделить с нами это веселье.
Приятно делать проект, которым пользуются другие люди.

Если ты знаешь питон или что-то про то как писать в вебе, мы предоставим тебе плацдарм для оттачивания этих навыков.

# Contributing

> TL;DR тут идет техничная часть

Прежде чем брать что-то реализовывать, дай нам об этом знать, написав пару строк в телеграм @shrimpsizemoose или @inn0kenty.

Мы очень любим и используем везде: [Python](https://www.python.org), [Docker](https://www.docker.com) и [SemVer](https://semver.org).

Мы считаем что явное всегда лучше неявного, стараемся придерживаться этого принципа во всем и хотим чтобы это всегда было так.

Все наши проекты пишутся и будут всегда писаться на python.
Ну может иногда нас кто-то кусает и мы что-то пилим на golang, но потом плюёмся полгода.
Если ты хочешь написать что-то, например, на haskell, то увы, мы не сможем это принять.
Люди приходят и уходят, а код, который надо поддерживать, остается.

У нас есть фронтенд.
Мы не умеем его писать и делаем это медленно.
Но если вдруг будем делать что-то с нуля то это будет какой-нибудь Vue и bootstrap.
В целом, если у вас встаёт дыбом внутренний пурист и вы считаете что jquery устарела и как же так можно их использовать в 202X году, то мы не сработаемся.

Ещё мы считаем, что буква ё &mdash; лучшая буква в русском алфавите.
И материмся в рабочих чатах.
Довольно часто.
Мы бы и в этом документе матерились, но моралисты сказали что можно выбрать или мат или букву ё, _и мы хоть убей не помним что же мы выбрали_.

А, ну и мы считаем себя прогрессивными и нас не волнует чья-либо раса, пол, и религиозные убеждения.
У любого человека должно быть право на любой невроз, поэтому любые ваши загоны нас абсолютно не волнуют.
Настоящая гонзо-разработка, как в сладких мечтах о том прошлом, которое мы потеряли.
Полная меритократия и киберпанк.
И сейфспейс.
Четыре сейфспейса, если быть точным, мы же питонисты как-никак.

Мы не учим гуглить, пользоваться гитом, или где искать правую кнопку мыши и, соответственно, ожидаем что этому не надо будет учить.
Если остались сомнения или просто хочется оценить свои силы, начать можно с текущих [issues](https://gitlab.com/groups/letnyayashkola/-/issues).
Там текущие задачи, но их надо уметь решать хотя бы на уровне "я кажется могу разобраться где это в коде и/или воспроизвести локально".

Ладно, вот теперь правда техническая правда о наших проектах.

### Релизы

Весь код собирается в docker образы и релизится нашим CI/CD. Релизы выкатываются с помощью [Ansible](https://www.ansible.com/). CI/CD запускается на пуш тега.

Образы каждого проекта лежат в GitLab Container Registry. При билде нового образа на него вешается два тега: latest и git'овский тег.

Наша рабочая и релизная ветка - master. Однако наш CI/CD умеет работать и с другими ветками.
Для релизов из других веток, мы используем в тегах release candidate (см. semver). Мы никогда не ставим теги rc в master ветке (ну или стараемся это делать).

Когда никто не видит, мы пушим в мастер. Форсом. Потому что можем, вот почему.
А ещё мы ходим на наши тачки под рутом. Но вас не пустим.

Поскольку master является еще и рабочей веткой, нет гарантий, что там лежит стабильный код.
Самый стабильный код можно всегда найти по зарелизенному в prod тегу.

#### Релизы сайтов

У сайтов есть два окружения staging и prod. CI/CD собирает образ и выкатывает его автоматически на staging.
Релиз на prod осуществляется руками (в смысле надо на кнопку рукой нажать).

#### Релизы ботов

Процесс похож на релиз сайтов. Только у ботов всего одно окружение. Формально это staging.


Если ты дочитал до сюда и не закрыл страницу, значит у тебя есть все шансы вписаться к нам.
